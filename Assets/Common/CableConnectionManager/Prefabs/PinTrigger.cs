﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinTrigger : MonoBehaviour
{
    float distanceToMove = 10f;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "LightSource")
        {
            other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.x, other.gameObject.transform.position.y + distanceToMove, other.gameObject.transform.position.z);
        }
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "LightSource")
        {
            other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.x, other.gameObject.transform.position.y + distanceToMove, other.gameObject.transform.position.z); ;
        }
    }
}
